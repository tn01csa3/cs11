TOPIC:
CREATE A SCALABLE REST ENGINE FOR E-COMMERCE SITE TO SERVE ANY FRONTEND CLIENT.


MEMBERS:
1) ANJALI SURENDRA SINGH   
2) SNEHA RAVINDRA JADHAV
3) APURVA UPADHYAY
4) SMITA AVHAD

ABSTRACT:
•REST( Representational State Transfer) is architectural style for designing loosely coupled web services. It is mainly used to develop lightweight, fast, scalable and easy to maintain web services that often use HTTP as means of communication. REST is not linked to any particular platform or technology. REST defines Web as a distributed hypermedia application 
•RESTful applications use HTTP request to post data(create/update), read data(making queries) and delete data. Hence REST uses Http for all four CRUD (Create/Read/Upadte/Delete).
•REST is not standard in itself but instead is an architectural style that uses standards like HTTP, XML,JSON etc. The REST  architectural style provides guiding principles for building.

